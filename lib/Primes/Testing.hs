module Primes.Testing where

import Data.List
import Control.Monad.State.Strict
import Control.Arrow
import Data.Maybe
import Data.Bits

import Moduli
import Fibonacci
import Powers.Squares
import Powers.Lucas

isPrime :: (Bits a, Integral a) => a -> Bool
isPrime = bpsw

bpsw :: (Bits a, Integral a) => a -> Bool
bspw 2 = True
bpsw n
    | isPerfectSquare n = False
    | otherwise = (mrpt 2 n) && ((u == 0) || (elem 0 vs))
        where
            d = head . dropWhile ((/= (-1)) . flip jacobi n) $ [5 + 2*n*(-1)^n | n <- [0..]] 
            q = (d - 1) `div` 4
            s = trailingZeroes n
            o = n `unsafeShiftR` s
            u = lucasUMod  (1, q) o n
            v = lucasVMod (1,q) o n
            vs = take s . map snd . iterate next $ (expMod n o q, v)
            next (qk, v) = (expMod n 2 qk, (expMod n 2 v - 2*qk) `mod` n)

psw :: (Bits a, Integral a) => a -> Bool
psw n = mrpt 2 n && (fibMod n (n+1) == 0 || (mod n 5 `notElem` [2, 3])) || (n == 2)

mrpt :: (Bits a, Integral a) => a -> a -> Bool
mrpt b p = maybe False (== 1) (listToMaybe list) || elem p' list
    where 
        p' = p - 1
        s = trailingZeroes p'
        d = p' `unsafeShiftR` s
        list = genericTake s . iterate (expMod p 2) $ expMod p d b

trailingZeroes :: (Integral a, Bits a, Integral b) => a -> b
trailingZeroes x = fromIntegral . popCount $ (x .&. (-x)) - 1

jacobi d n
    | n > d = unsafeJacobi d n
    | otherwise = unsafeJacobi (d `mod` n) n

-- n > d
unsafeJacobi :: (Bits a, Integral a) => a -> a -> a
unsafeJacobi 1 n = 1
unsafeJacobi d n
    | even d = case trailingZeroes d of
                 z | even z -> 1
                   | odd z -> (-1)^(((n ^ 2) - 1) `div` 8)
    | unsafeGcd n d /= 1 = 0
    | otherwise = (-1)^((d - 1) * (n - 1) `div` 4) `div` (unsafeJacobi (n `mod` d) d)

-- x > y
unsafeGcd :: (Integral a) => a -> a -> a
unsafeGcd x y = case x `mod` y of
                  0  -> y
                  x' -> unsafeGcd y x'
