module Powers.General where

-- | @pow (*) k x n@ computes @k * x^n@, where @(*)@ is an
-- associative binary function, and @x^n@ means @x*x*x...@,
-- repeated @n@ times
pow (*) = loop
  where
    loop k x n
      | d == 0    = k'
      | otherwise = loop k' (x * x) d
      where
        (d, m) = n `divMod` 2
        k' | m == 0    = k
           | otherwise = k * x