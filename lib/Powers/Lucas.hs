module Powers.Lucas (lucas, lucasU, lucasV, lucasMod, lucasUMod, lucasVMod) where

import Powers.General

-- | @lucas (p,q) (a,b) n@ is the @n@-th term of the Lucas
-- sequence with parameters @(p,q)@ and initial terms @(a,b)@
lucas (p,q) = go
  where
    mul (a,b) (c,d) = (p*a*c - q*a*d - q*b*c, a*c - q*b*d)
    go k 0 = snd k
    go k n = fst $ pow mul k (1,0) (n-1)

lucasMod (p,q) x n m = go x n
  where
    mul (a,b) (c,d) = ((p*a*c - q*a*d - q*b*c) `mod` m, (a*c - q*b*d) `mod` m)
    go k 0 = snd k `mod` m
    go k n = fst $ pow mul k (1,0) (n-1)

lucasU (p,q) = lucas (p,q) (1,0)
lucasV (p,q) = lucas (p,q) (p,2)

lucasUMod (p,q) = lucasMod (p,q) (1,0)
lucasVMod (p,q) = lucasMod (p,q) (p,2)
