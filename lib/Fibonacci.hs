module Fibonacci (fibMod) where

fibMod m n = (\(_,x,_,_) -> x) . matrixExpMod m n $ (1,1,1,0)

matrixExpMod :: (Integral a) => a -> a -> (a,a,a,a) -> (a,a,a,a)
matrixExpMod m n x@(a,b,c,d) = go n
    where
        go 0 = (1,0,0,1)
        go 1 = (a `mod` m,b `mod` m,c `mod` m,d `mod` m)
        go n
          | even n = squareMod $ go (n `div` 2)
          | otherwise = x *% go (n - 1)
        (a,b,c,d) *% (w,x,y,z) = ((a*w + b*y) `mod` m, (a*x + b*z) `mod` m
                                 ,(c*w + d*y) `mod` m, (c*x + d*z))
        squareMod (a,b,c,d) = ((a*a + b*c) `mod` m, (b*(a + d)) `mod` m
                             ,(c*(a + d)) `mod` m, (d*d + b*c) `mod` m)
